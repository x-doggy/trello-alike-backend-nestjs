/**
 * @package user
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class user.entity
 */
import { ApiProperty } from "@nestjs/swagger";
import { Exclude } from "class-transformer";
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { CardColumn } from "~/card-column/card-column.entity";
import { compare, hash } from "~/common/hashing.service";
import { Role, ROLES } from "~/role/role.entity";

const tableName = "users";

@Entity(tableName)
export class User {
  public static tableName = tableName;

  @PrimaryGeneratedColumn("uuid", { name: "id", comment: "user id" })
  public id!: string;

  @Column("varchar", {
    name: "email",
    unique: true,
    nullable: false,
    length: 255,
    comment: "user email",
    transformer: {
      from: (val: string) => val,
      to: (val: string) => val.toLowerCase(),
    },
  })
  @ApiProperty()
  public email!: string;

  @ManyToMany(() => Role, (r: Role) => r.id, {
    eager: true,
    cascade: ["insert", "update", "remove"],
  })
  @JoinTable({
    name: "user_roles",
    joinColumn: {
      name: "role_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "user_id",
      referencedColumnName: "id",
    },
  })
  public userRoles?: Role[];

  @Column("varchar", {
    name: "password",
    length: 255,
    comment: "user password",
  })
  @ApiProperty()
  @Exclude()
  public password!: string;

  @CreateDateColumn({
    name: "create_timestamp",
    comment: "user creating date",
  })
  public createTimestamp!: Date | string;

  @UpdateDateColumn({
    name: "update_timestamp",
    comment: "user updating date",
  })
  public updateTimestamp!: Date | string;

  @OneToMany(() => CardColumn, (c: CardColumn) => c.owner)
  public cardColumns?: CardColumn[];

  @BeforeInsert()
  public encryptPassword(): void {
    this.password = hash(this.password);
  }

  public validated(password: string): boolean {
    return compare(password, this.password);
  }

  get stringRoles(): string[] {
    return Array.isArray(this.userRoles)
      ? this.userRoles.map((role: Role) => role.name)
      : [];
  }

  get role(): ROLES {
    return this.stringRoles.includes(ROLES.ADMIN) ? ROLES.ADMIN : ROLES.USER;
  }

  get isAdmin(): boolean {
    return this.role === ROLES.ADMIN;
  }

  get isUser(): boolean {
    return this.role === ROLES.USER;
  }
}
