/**
 * @package user
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file user.dto
 */
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsDefined,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from "class-validator";

import { ROLES } from "~/role/role.entity";
import { User } from "~/user/user.entity";

export class UserLoginDto {
  @IsEmail({}, { message: "E-mail is not matched" })
  @IsDefined()
  @IsNotEmpty()
  @ApiProperty()
  public readonly email!: string;

  @IsString()
  @IsDefined()
  @IsNotEmpty()
  @ApiProperty()
  public readonly password!: string;
}

export class UserCreateDto extends UserLoginDto {
  @IsEnum(ROLES, { message: "Role doesn't match enum" })
  @IsOptional()
  @ApiPropertyOptional({ example: ROLES.USER })
  public readonly role?: ROLES;
}

export class UserLoginResponseDto {
  @ApiProperty({ description: "User itself", type: () => User })
  public user!: User;

  @ApiProperty({
    description: "Access token",
    example: "81975e36-94bc-49e6-be8b-c529fab1c403",
  })
  public accessToken!: string;
}
