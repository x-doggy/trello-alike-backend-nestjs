/**
 * @package user
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class user.service
 */
import {
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from "@nestjs/common";
import { JwtService, JwtSignOptions } from "@nestjs/jwt";
import { classToPlain, plainToClass } from "class-transformer";

import { RoleRepository } from "~/role/role.repository";
import { UserCreateDto, UserLoginDto, UserLoginResponseDto } from "~/user/user.dto";
import { User } from "~/user/user.entity";
import { UserRepository } from "~/user/user.repository";

@Injectable()
export class UserService {
  public constructor(
    private readonly userRepository: UserRepository,
    private readonly roleRepository: RoleRepository,
    private readonly jwtService: JwtService,
  ) {}

  public async findOne(id: string): Promise<User | never> {
    try {
      return await this.userRepository.findById(id);
    } catch (e) {
      throw new NotFoundException();
    }
  }

  public async validateUser(email: string, password: string): Promise<User | never> {
    const userFound: User | undefined = await this.userRepository.findByEmail(email);

    if (!userFound || !userFound.validated(password)) {
      throw new UnprocessableEntityException("Invalid credentials");
    }

    return userFound;
  }

  public signToken(user: User): UserLoginResponseDto {
    return plainToClass(UserLoginResponseDto, {
      user: classToPlain<User>(user),
      accessToken: this.jwtService.sign(
        {
          _id: user.id,
          email: user.email,
          role: user.role,
        },
        { subject: user.id } as Partial<JwtSignOptions>,
      ),
    });
  }

  public async create(userData: UserCreateDto): Promise<User | never> {
    const userByEmailCount = await this.userRepository.countByEmail(userData.email);

    if (userByEmailCount > 0) {
      throw new UnprocessableEntityException("User already exists");
    }

    const userRoles = userData.role
      ? [await this.roleRepository.findByName(userData.role)]
      : [await this.roleRepository.findUserRole()];

    const actualUser: User = this.userRepository.create({
      email: userData.email,
      password: userData.password,
      userRoles,
    });

    await this.userRepository.save(actualUser);
    return await this.userRepository.findById(actualUser.id);
  }

  public async login(userData: UserLoginDto): Promise<UserLoginResponseDto | never> {
    const user: User = await this.validateUser(userData.email, userData.password);
    return this.signToken(user);
  }
}
