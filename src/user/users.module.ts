/**
 * @package user
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class users.module
 */
import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { TypeOrmModule } from "@nestjs/typeorm";

import { AuthModule } from "~/auth/auth.module";
import { JwtStrategy } from "~/auth/jwt/jwt.strategy";
import { LocalStrategy } from "~/auth/local/local.strategy";
import { Role } from "~/role/role.entity";
import { RoleRepository } from "~/role/role.repository";
import { UserController } from "~/user/user.controller";
import { User } from "~/user/user.entity";
import { UserRepository } from "~/user/user.repository";
import { UserService } from "~/user/user.service";

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, UserRepository, RoleRepository]),
    AuthModule,
    PassportModule,
  ],
  controllers: [UserController],
  providers: [LocalStrategy, JwtStrategy, UserService],
})
export class UserModule {}
