/**
 * @package user
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class user.controller
 */
import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Post,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from "@nestjs/swagger";

import { JwtAuthGuard } from "~/auth/jwt/jwt-auth.guard";
import { LocalAuthGuard } from "~/auth/local/local-auth.guard";
import { Public } from "~/common/public.decorator";
import { UserCreateDto, UserLoginDto, UserLoginResponseDto } from "~/user/user.dto";
import { User } from "~/user/user.entity";
import { UserService } from "~/user/user.service";

@Controller("/api/v1/users")
@ApiTags("User")
export class UserController {
  public constructor(private readonly userService: UserService) {}

  @Get(":userId")
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ summary: "Get Single User" })
  @ApiOkResponse({ description: "User" })
  @ApiUnauthorizedResponse({ description: "Not authorized" })
  @ApiNotFoundResponse({ description: "Not Found" })
  public findOne(
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
  ): Promise<User | never> {
    return this.userService.findOne(userId);
  }

  @Post()
  @Public()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: "User Register" })
  @ApiOkResponse({ description: "User" })
  @ApiBadRequestResponse({ description: "Incorrect Data" })
  @ApiUnprocessableEntityResponse({ description: "User already exists" })
  public register(@Body() userData: UserCreateDto): Promise<User | never> {
    return this.userService.create(userData);
  }

  @Post("login")
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: "User Login" })
  @ApiBody({ type: () => UserLoginDto })
  @ApiOkResponse({ type: () => UserLoginResponseDto, description: "User" })
  @ApiUnauthorizedResponse({
    description: "Email is already used or password is incorrect",
  })
  public login(@Body() userData: UserLoginDto): Promise<UserLoginResponseDto | never> {
    return this.userService.login(userData);
  }
}
