/**
 * @package user
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class user.repository
 */
import { EntityRepository, Repository, SelectQueryBuilder } from "typeorm";

import { Role } from "~/role/role.entity";
import { User } from "~/user/user.entity";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  public select(): SelectQueryBuilder<User> {
    return this.createQueryBuilder(User.tableName)
      .select([
        `${User.tableName}.id`,
        `${User.tableName}.email`,
        `${Role.tableName}.id`,
        `${Role.tableName}.name`,
      ])
      .leftJoin(`${User.tableName}.userRoles`, Role.tableName);
  }

  public findById(id: string): Promise<User | never> {
    return this.select().where(`${User.tableName}.id = :id`, { id }).getOneOrFail();
  }

  public findByEmail(email: string): Promise<User | never> {
    return this.select()
      .addSelect(`${User.tableName}.password`)
      .where(`${User.tableName}.email = :email`, { email })
      .getOneOrFail();
  }

  public countByEmail(email: string): Promise<number> {
    return this.createQueryBuilder(User.tableName)
      .where(`${User.tableName}.email = :email`, { email })
      .getCount();
  }
}
