/**
 * @package card-column
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file card-column.dto
 */
import { ApiProperty, ApiPropertyOptional, OmitType } from "@nestjs/swagger";
import {
  IsDefined,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from "class-validator";

export class CardColumnCreateDto {
  @IsString({ message: "Title is not a string" })
  @IsDefined({ message: "Title is not defined" })
  @IsNotEmpty({ message: "Title is empty" })
  @ApiProperty()
  public readonly title!: string;

  @IsUUID(4, { message: "Owner ID doesn't match UUID" })
  @IsDefined({ message: "Owner is not defined" })
  @IsNotEmpty({ message: "Owner is empty" })
  @ApiProperty({ example: "fd3f2ab7-cbc2-470e-b0e6-4ff31f9c9070" })
  public readonly ownerId!: string;

  @IsInt({ message: "Position is not a number" })
  @IsOptional()
  @ApiPropertyOptional()
  public readonly position?: number | undefined;
}

export class CardColumnEditDto extends CardColumnCreateDto {
  @IsUUID(4, { message: "Column Id doesn't match UUID" })
  @IsDefined({ message: "Column Id is not defined" })
  @IsNotEmpty({ message: "Column Id is empty" })
  @ApiProperty()
  public id!: string;
}

export class CardColumnCreateBody extends OmitType(CardColumnCreateDto, [
  "ownerId",
] as const) {}

export class CardColumnEditBody extends OmitType(CardColumnEditDto, [
  "id",
  "ownerId",
] as const) {}
