/**
 * @package card-column
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card-column.service
 */
import { Injectable, NotFoundException } from "@nestjs/common";
import { DeleteResult } from "typeorm";

import { CardColumnCreateDto, CardColumnEditDto } from "~/card-column/card-column.dto";
import { CardColumn } from "~/card-column/card-column.entity";
import { CardColumnRepository } from "~/card-column/card-column.repository";
import { UserRepository } from "~/user/user.repository";

@Injectable()
export class CardColumnService {
  public constructor(
    private readonly cardColumnRepository: CardColumnRepository,
    private readonly userRepository: UserRepository,
  ) {}

  public findByOwnerId(ownerId: string): Promise<CardColumn[]> {
    return this.cardColumnRepository.findByOwnerId(ownerId);
  }

  public findOne(ownerId: string, columnId: string): Promise<CardColumn | undefined> {
    return this.cardColumnRepository.findOneByOwnerId(ownerId, columnId);
  }

  public async create(cardColumnData: CardColumnCreateDto): Promise<CardColumn> {
    const cardColumn: CardColumn = this.cardColumnRepository.create({
      ...cardColumnData,
      owner: await this.userRepository.findOne(cardColumnData.ownerId),
    });
    const savedCardColumn = await this.cardColumnRepository.save(cardColumn);
    return await this.cardColumnRepository.findOneByOwnerId(
      savedCardColumn.ownerId,
      savedCardColumn.id,
    );
  }

  public async edit(cardColumnData: CardColumnEditDto): Promise<CardColumn | never> {
    const originCardColumn: CardColumn | undefined =
      await this.cardColumnRepository.findOneByOwnerId(
        cardColumnData.ownerId,
        cardColumnData.id,
      );

    if (!originCardColumn) {
      throw new NotFoundException("Not found");
    }

    const cardColumn: CardColumn = this.cardColumnRepository.merge(originCardColumn, {
      id: cardColumnData.id,
      owner: await this.userRepository.findOne(cardColumnData.ownerId),
      title: cardColumnData.title,
      position: cardColumnData.position,
    });

    const savedCardColumn = await this.cardColumnRepository.save(cardColumn);
    return await this.cardColumnRepository.findOneByOwnerId(
      savedCardColumn.ownerId,
      savedCardColumn.id,
    );
  }

  public deleteById(ownerId: string, columnId: string): Promise<DeleteResult> {
    return this.cardColumnRepository.deleteById(ownerId, columnId);
  }

  public async checkColumnOwn(userId: string, columnId: string): Promise<boolean> {
    const foundColumn = await this.cardColumnRepository.findOne(columnId);

    if (!foundColumn || !foundColumn.owner) {
      return false;
    }

    return foundColumn.owner?.id === userId;
  }
}
