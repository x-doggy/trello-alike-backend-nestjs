/**
 * @package card-column
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card-column.controller
 */
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from "@nestjs/swagger";
import { plainToClass } from "class-transformer";
import { DeleteResult } from "typeorm";

import { JwtAuthGuard } from "~/auth/jwt/jwt-auth.guard";
import {
  CardColumnCreateBody,
  CardColumnCreateDto,
  CardColumnEditBody,
  CardColumnEditDto,
} from "~/card-column/card-column.dto";
import { CardColumn } from "~/card-column/card-column.entity";
import { CardColumnGuard } from "~/card-column/card-column.guard";
import { CardColumnService } from "~/card-column/card-column.service";

@Controller("/api/v1/users/:userId/columns")
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags("Column")
export class CardColumnController {
  public constructor(private readonly cardColumnService: CardColumnService) {}

  @Get()
  @ApiOperation({ summary: "Get Columns" })
  @ApiOkResponse({ description: "Columns" })
  public findAll(
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
  ): Promise<CardColumn[]> {
    return this.cardColumnService.findByOwnerId(userId);
  }

  @Get(":columnId")
  @ApiOperation({ summary: "Get Column" })
  @ApiOkResponse({ description: "Column" })
  public findOne(
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
  ): Promise<CardColumn | undefined> {
    return this.cardColumnService.findOne(userId, columnId);
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  @ApiBody({ type: () => CardColumnCreateBody })
  @ApiOperation({ summary: "Create Column" })
  @ApiOkResponse({ description: "Column" })
  @ApiBadRequestResponse({ description: "Incorrect Data" })
  public create(
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
    @Body() cardColumnData: CardColumnEditBody,
  ): Promise<CardColumn | never> {
    return this.cardColumnService.create(
      plainToClass(CardColumnCreateDto, { ...cardColumnData, ownerId: userId }),
    );
  }

  @Patch(":columnId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(CardColumnGuard)
  @ApiOperation({ summary: "Edit Column" })
  @ApiBody({ type: () => CardColumnEditBody })
  @ApiOkResponse({ description: "Column" })
  @ApiNotFoundResponse({ description: "Not found" })
  @ApiForbiddenResponse({ description: "Access is forbidden" })
  public edit(
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Body() cardColumnData: CardColumnEditBody,
  ): Promise<CardColumn | never> {
    return this.cardColumnService.edit(
      plainToClass(CardColumnEditDto, {
        ...cardColumnData,
        id: columnId,
        ownerId: userId,
      }),
    );
  }

  @Delete(":columnId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(CardColumnGuard)
  @ApiOperation({ summary: "Delete Column" })
  @ApiOkResponse()
  public deleteById(
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
  ): Promise<DeleteResult> {
    return this.cardColumnService.deleteById(userId, columnId);
  }
}
