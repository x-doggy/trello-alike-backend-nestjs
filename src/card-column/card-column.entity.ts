/**
 * @package card-column
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card-column.entity
 */
import { ApiProperty } from "@nestjs/swagger";
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm";

import { Card } from "~/card/card.entity";
import { User } from "~/user/user.entity";

const tableName = "columns";

@Entity(tableName)
@Unique(["position"])
export class CardColumn {
  public static tableName = tableName;

  @PrimaryGeneratedColumn("uuid", { name: "id", comment: "column id" })
  @ApiProperty()
  public id!: string;

  @Column("varchar", {
    name: "title",
    length: 255,
    comment: "column title",
  })
  @ApiProperty()
  public title!: string;

  @Column("int", {
    name: "position",
    width: 255,
    comment: "column position",
  })
  @Generated("increment")
  @ApiProperty()
  public position!: number;

  @CreateDateColumn({
    name: "create_timestamp",
    comment: "column creating date",
  })
  public createTimestamp!: Date | string;

  @UpdateDateColumn({
    name: "update_timestamp",
    comment: "column updating date",
  })
  public updateTimestamp!: Date | string;

  @Column({ nullable: false })
  public ownerId!: string;

  @ManyToOne(() => User, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    cascade: true,
    nullable: false,
  })
  @JoinColumn({ name: "ownerId", referencedColumnName: "id" })
  public owner?: User;

  @OneToMany(() => Card, (c: Card) => c.cardColumn)
  public cards?: Card[];
}
