/**
 * @package card-column
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card-column.repository
 */
import { DeleteResult, EntityRepository, Repository, SelectQueryBuilder } from "typeorm";

import { CardColumn } from "~/card-column/card-column.entity";

@EntityRepository(CardColumn)
export class CardColumnRepository extends Repository<CardColumn> {
  public select(): SelectQueryBuilder<CardColumn> {
    return this.createQueryBuilder(CardColumn.tableName).select([
      `${CardColumn.tableName}.id`,
      `${CardColumn.tableName}.title`,
      `${CardColumn.tableName}.position`,
      `${CardColumn.tableName}.createTimestamp`,
      `${CardColumn.tableName}.updateTimestamp`,
    ]);
  }

  public findByOwnerId(userId: string): Promise<CardColumn[]> {
    return this.select()
      .where(`${CardColumn.tableName}.ownerId = :userId`, { userId })
      .orderBy(`${CardColumn.tableName}.position`, "ASC")
      .getMany();
  }

  public findOneByOwnerId(userId: string, columnId: string): Promise<CardColumn | never> {
    return this.select()
      .where(`${CardColumn.tableName}.id = :columnId`)
      .andWhere(`${CardColumn.tableName}.ownerId = :userId`, { columnId, userId })
      .getOneOrFail();
  }

  public deleteById(userId: string, columnId: string): Promise<DeleteResult> {
    return this.createQueryBuilder(CardColumn.tableName)
      .delete()
      .from(CardColumn)
      .where("id = :columnId", { columnId })
      .andWhere("ownerId = :userId", { userId })
      .execute();
  }
}
