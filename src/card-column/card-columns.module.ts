/**
 * @package modules
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card-columns.module
 */
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { CardColumnController } from "~/card-column/card-column.controller";
import { CardColumn } from "~/card-column/card-column.entity";
import { CardColumnRepository } from "~/card-column/card-column.repository";
import { CardColumnService } from "~/card-column/card-column.service";
import { User } from "~/user/user.entity";
import { UserRepository } from "~/user/user.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature([User, CardColumn, CardColumnRepository, UserRepository]),
  ],
  controllers: [CardColumnController],
  providers: [CardColumnService],
})
export class CardColumnModule {}
