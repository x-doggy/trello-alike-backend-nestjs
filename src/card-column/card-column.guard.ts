/**
 * @package card-column
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card-column.guard
 */
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";

import { CardColumnService } from "~/card-column/card-column.service";
import { ROLES } from "~/role/role.entity";

@Injectable()
export class CardColumnGuard implements CanActivate {
  public constructor(
    private readonly reflector: Reflector,
    private readonly cardColumnService: CardColumnService,
  ) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const authUser = request.user;
    if (authUser.role === ROLES.ADMIN) {
      return true;
    }

    const userId = request.params?.userId;
    const columnId = request.params?.columnId;
    if (!userId || !columnId) {
      return false;
    }

    return await this.cardColumnService.checkColumnOwn(userId, columnId);
  }
}
