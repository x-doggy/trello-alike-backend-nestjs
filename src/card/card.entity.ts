/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card.entity
 */
import { ApiProperty } from "@nestjs/swagger";
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm";

import { CardColumn } from "~/card-column/card-column.entity";
import { Comment } from "~/comment/comment.entity";

const tableName = "cards";

@Entity(tableName)
@Unique(["position"])
export class Card {
  public static tableName = tableName;

  @PrimaryGeneratedColumn("uuid", { name: "id", comment: "card id" })
  @ApiProperty()
  public id!: string;

  @Column("varchar", {
    name: "title",
    length: 255,
    nullable: true,
    default: "",
    comment: "card title",
  })
  @ApiProperty()
  public title?: string | null;

  @Column("text", {
    name: "description",
    nullable: true,
    default: "",
    comment: "card description",
  })
  @ApiProperty()
  public description?: string | null;

  @Column("int", {
    name: "position",
    width: 255,
    comment: "card position",
  })
  @Generated("increment")
  @ApiProperty()
  public position!: number;

  @CreateDateColumn({
    name: "create_timestamp",
    comment: "card creating date",
  })
  public createTimestamp!: Date | string;

  @UpdateDateColumn({
    name: "update_timestamp",
    comment: "card updating date",
  })
  public updateTimestamp!: Date | string;

  @Column({ nullable: false })
  public cardColumnId!: string;

  @ManyToOne(() => CardColumn, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    cascade: true,
    nullable: false,
  })
  @JoinColumn({ name: "cardColumnId", referencedColumnName: "id" })
  public cardColumn!: CardColumn;

  @OneToMany(() => Comment, (c: Comment) => c.card)
  public comments?: Comment[];
}
