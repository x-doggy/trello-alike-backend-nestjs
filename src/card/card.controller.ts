/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card.controller
 */
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from "@nestjs/swagger";
import { plainToClass } from "class-transformer";
import { DeleteResult } from "typeorm";

import { JwtAuthGuard } from "~/auth/jwt/jwt-auth.guard";
import { CardCreateBody, CardCreateDto } from "~/card/card.dto";
import { CardEditBody, CardEditDto } from "~/card/card.dto";
import { Card } from "~/card/card.entity";
import { CardGuard } from "~/card/card.guard";
import { CardService } from "~/card/card.service";

@Controller("/api/v1/users/:userId/columns/:columnId/cards")
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags("Card")
export class CardController {
  public constructor(private readonly cardService: CardService) {}

  @Get()
  @ApiOperation({ summary: "Get Cards" })
  @ApiOkResponse({ description: "Cards" })
  public findAll(
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
  ): Promise<Card[]> {
    return this.cardService.findByColumnId(columnId, userId);
  }

  @Get(":cardId")
  @ApiOperation({ summary: "Get Card" })
  @ApiOkResponse({ description: "Card" })
  @ApiNotFoundResponse({ description: "Not found" })
  public findOne(
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
  ): Promise<Card | undefined> {
    return this.cardService.findOne(cardId, columnId, userId);
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: "Create Card" })
  @ApiBody({ type: () => CardCreateBody })
  @ApiOkResponse({ description: "Card" })
  @ApiBadRequestResponse({ description: "Incorrect data" })
  public create(
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) ownerId: string,
    @Body() cardData: CardCreateBody,
  ): Promise<Card> {
    return this.cardService.create(
      plainToClass(CardCreateDto, { ...cardData, columnId, ownerId }),
    );
  }

  @Patch(":cardId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(CardGuard)
  @ApiOperation({ summary: "Edit Card" })
  @ApiBody({ type: () => CardEditBody })
  @ApiOkResponse({ description: "Card" })
  @ApiNotFoundResponse({ description: "Not found" })
  @ApiForbiddenResponse({ description: "Access is forbidden" })
  public edit(
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
    @Body() cardData: CardEditBody,
  ): Promise<Card | never> {
    return this.cardService.edit(
      plainToClass(CardEditDto, { ...cardData, cardId, columnId, ownerId: userId }),
    );
  }

  @Delete(":cardId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(CardGuard)
  @ApiOperation({ summary: "Delete Card" })
  @ApiOkResponse()
  public deleteById(
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) userId: string,
  ): Promise<DeleteResult> {
    return this.cardService.deleteById(cardId, columnId, userId);
  }
}
