/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file card.dto
 */
import { ApiProperty, ApiPropertyOptional, OmitType } from "@nestjs/swagger";
import {
  IsDefined,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from "class-validator";

export class CardCreateDto {
  @IsString({ message: "Title is not a string" })
  @IsOptional()
  @ApiPropertyOptional()
  public readonly title?: string | undefined;

  @IsString({ message: "Description is not a string" })
  @IsOptional()
  @ApiPropertyOptional()
  public readonly description?: string | undefined;

  @IsInt({ message: "Position is not a number" })
  @IsOptional()
  @ApiPropertyOptional()
  public readonly position?: number | undefined;

  @IsUUID(4, { message: "Column Id doesn't match UUID" })
  @IsDefined({ message: "Column Id is not defined" })
  @IsNotEmpty({ message: "Column Id is empty" })
  @ApiProperty({ example: "0f48253d-43ce-42bb-ad8b-e900379921ed" })
  public readonly columnId!: string;

  @IsUUID(4, { message: "Owner Id doesn't match UUID" })
  @IsDefined({ message: "Owner Id is not defined" })
  @IsNotEmpty({ message: "Owner Id is empty" })
  @ApiProperty({ example: "fd3f2ab7-cbc2-470e-b0e6-4ff31f9c9070" })
  public readonly ownerId!: string;
}

export class CardEditDto extends CardCreateDto {
  @IsUUID(4, { message: "Card Id doesn't match UUID" })
  @IsDefined({ message: "Card Id is not defined" })
  @IsNotEmpty({ message: "Card Id is empty" })
  @ApiProperty({ example: "02fba532-d0ff-49bf-809d-1db5ca800aca" })
  public readonly cardId!: string;
}

export class CardCreateBody extends OmitType(CardCreateDto, [
  "columnId",
  "ownerId",
] as const) {}

export class CardEditBody extends OmitType(CardEditDto, [
  "cardId",
  "columnId",
  "ownerId",
] as const) {}
