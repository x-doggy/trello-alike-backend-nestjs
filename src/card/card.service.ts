/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card.service
 */
import { Injectable, UnprocessableEntityException } from "@nestjs/common";
import { DeleteResult } from "typeorm";

import { CardCreateDto, CardEditDto } from "~/card/card.dto";
import { Card } from "~/card/card.entity";
import { CardRepository } from "~/card/card.repository";
import { CardColumnRepository } from "~/card-column/card-column.repository";

@Injectable()
export class CardService {
  public constructor(
    private readonly cardRepository: CardRepository,
    private readonly cardColumnRepository: CardColumnRepository,
  ) {}

  public findByColumnId(columnId: string, ownerId: string): Promise<Card[]> {
    return this.cardRepository.findByColumnId(columnId, ownerId);
  }

  public findOne(cardId: string, columnId: string, ownerId: string): Promise<Card> {
    return this.cardRepository.findOneByColumnId(cardId, columnId, ownerId);
  }

  public deleteById(
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<DeleteResult> {
    return this.cardRepository.deleteById(cardId, columnId, ownerId);
  }

  public async create(cardData: CardCreateDto): Promise<Card> {
    const card: Card = this.cardRepository.create({
      ...cardData,
      cardColumn: await this.cardColumnRepository.findOne(cardData.columnId),
    });
    const savedCard = await this.cardRepository.save(card);
    return await this.cardRepository.findOneByColumnId(
      savedCard.id,
      savedCard.cardColumnId,
      savedCard.cardColumn.ownerId,
    );
  }

  public async edit(cardData: CardEditDto): Promise<Card | never> {
    const originCard: Card | undefined = await this.cardRepository.findOneByColumnId(
      cardData.cardId,
      cardData.columnId,
      cardData.ownerId,
    );

    if (!originCard) {
      throw new UnprocessableEntityException("Not found");
    }

    const card: Card = this.cardRepository.merge(originCard, {
      ...cardData,
      id: cardData.cardId,
      cardColumn: await this.cardColumnRepository.findOneByOwnerId(
        cardData.ownerId,
        cardData.columnId,
      ),
    });

    const savedCard = await this.cardRepository.save(card);
    return await this.cardRepository.findOneByColumnId(
      savedCard.id,
      savedCard.cardColumnId,
      savedCard.cardColumn.ownerId,
    );
  }

  public async checkCardOwn(
    cardId: string,
    columnId: string,
    userId: string,
  ): Promise<boolean> {
    const foundCard = await this.cardRepository.findOne(cardId);

    if (!foundCard || !foundCard.cardColumn || !foundCard.cardColumn.owner) {
      return false;
    }

    return (
      foundCard.cardColumn.id === columnId && foundCard.cardColumn.owner.id === userId
    );
  }
}
