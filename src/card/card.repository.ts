/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card.repository
 */
import { NotFoundException } from "@nestjs/common";
import { DeleteResult, EntityRepository, Repository, SelectQueryBuilder } from "typeorm";

import { Card } from "~/card/card.entity";
import { CardColumn } from "~/card-column/card-column.entity";

@EntityRepository(Card)
export class CardRepository extends Repository<Card> {
  public select(): SelectQueryBuilder<Card> {
    return this.createQueryBuilder(Card.tableName)
      .select([
        `${Card.tableName}.id`,
        `${Card.tableName}.title`,
        `${Card.tableName}.description`,
        `${Card.tableName}.position`,
        `${Card.tableName}.createTimestamp`,
        `${Card.tableName}.updateTimestamp`,
      ])
      .leftJoin(`${Card.tableName}.cardColumn`, CardColumn.tableName);
  }

  public findByColumnId(columnId: string, userId: string): Promise<Card[]> {
    return this.select()
      .where(`${Card.tableName}.cardColumnId = :columnId`, { columnId })
      .andWhere(`${CardColumn.tableName}.ownerId = :userId`, { userId })
      .orderBy(`${Card.tableName}.position`, "ASC")
      .getMany();
  }

  public findOneByColumnId(
    cardId: string,
    columnId: string,
    userId: string,
  ): Promise<Card> {
    return this.select()
      .where(`${Card.tableName}.id = :cardId`, { cardId })
      .andWhere(`${Card.tableName}.cardColumnId = :columnId`, { columnId })
      .andWhere(`${CardColumn.tableName}.ownerId = :userId`, { userId })
      .getOneOrFail();
  }

  public async deleteById(
    cardId: string,
    columnId: string,
    userId: string,
  ): Promise<DeleteResult> {
    const cardCount = await this.createQueryBuilder(Card.tableName)
      .leftJoin(`${Card.tableName}.cardColumn`, CardColumn.tableName)
      .where(`${Card.tableName}.cardColumnId = :columnId`, { columnId })
      .andWhere(`${CardColumn.tableName}.ownerId = :userId`, { userId })
      .getCount();
    if (cardCount) {
      return await this.createQueryBuilder(Card.tableName)
        .delete()
        .from(Card)
        .where("id = :cardId", { cardId })
        .andWhere("cardColumnId = :columnId", { columnId })
        .execute();
    }
    throw new NotFoundException();
  }
}
