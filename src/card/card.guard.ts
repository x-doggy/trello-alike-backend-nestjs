/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class card.guard
 */
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";

import { CardService } from "~/card/card.service";
import { ROLES } from "~/role/role.entity";

@Injectable()
export class CardGuard implements CanActivate {
  public constructor(
    private readonly reflector: Reflector,
    private readonly cardService: CardService,
  ) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const authUser = request.user;
    if (authUser.role === ROLES.ADMIN) {
      return true;
    }

    const userId = request.params?.userId;
    const columnId = request.params?.columnId;
    const cardId = request.params?.cardId;

    if (!userId || !columnId || !cardId) {
      return false;
    }

    return await this.cardService.checkCardOwn(cardId, columnId, userId);
  }
}
