/**
 * @package card
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class cards.module
 */
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { CardController } from "~/card/card.controller";
import { Card } from "~/card/card.entity";
import { CardRepository } from "~/card/card.repository";
import { CardService } from "~/card/card.service";
import { CardColumn } from "~/card-column/card-column.entity";
import { CardColumnRepository } from "~/card-column/card-column.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature([Card, CardColumn, CardRepository, CardColumnRepository]),
  ],
  controllers: [CardController],
  providers: [CardService],
})
export class CardModule {}
