/**
 * @package
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class app.module
 */
import { ClassSerializerInterceptor, Module, ValidationPipe } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { APP_FILTER, APP_INTERCEPTOR, APP_PIPE } from "@nestjs/core";

import { AuthModule } from "~/auth/auth.module";
import { CardModule } from "~/card/cards.module";
import { CardColumnModule } from "~/card-column/card-columns.module";
import { CommentModule } from "~/comment/comments.module";
import { HttpExceptionFilter } from "~/common/http-exception.filter";
import { DatabaseModule } from "~/database/database.module";
import { validate } from "~/env/env.validation";
import { UserModule } from "~/user/users.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validate,
    }),
    DatabaseModule,
    AuthModule,
    UserModule,
    CardColumnModule,
    CardModule,
    CommentModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_PIPE,
      useFactory: () => new ValidationPipe({ transform: true }),
    },
  ],
})
export class AppModule {}
