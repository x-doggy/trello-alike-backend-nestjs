/**
 * @package common
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file hashing.service
 */
import * as bcrypt from "bcryptjs";

function genSalt(): string {
  return bcrypt.genSaltSync(10);
}

export function hash(data: string): string {
  return bcrypt.hashSync(data, genSalt());
}

export function compare(origin: string, data: string): boolean {
  return bcrypt.compareSync(origin, data);
}

export function base64Hash(data: string): string {
  return Buffer.from(hash(data)).toString("base64");
}
