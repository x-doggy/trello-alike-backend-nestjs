/**
 * @package common
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file public.decorator
 */
import { SetMetadata } from "@nestjs/common";

export const IS_PUBLIC_KEY = "isPublic";
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);
