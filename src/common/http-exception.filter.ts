/**
 * @package common
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class http-exception.filter
 */
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from "@nestjs/common";
import { EntityNotFoundError } from "typeorm";

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  public async catch(exception: Record<string, any>, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    const status =
      exception instanceof EntityNotFoundError
        ? HttpStatus.NOT_FOUND
        : exception instanceof HttpException
        ? exception.getStatus()
        : exception.status || exception.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;

    const payload: { name: string; message: string; stackTrace?: string | string[] } = {
      name: exception.constructor.name,
      message: exception.message,
    };

    if (process.env.NODE_ENV === "development") {
      payload.stackTrace =
        exception.stack && exception.stack.includes("\n")
          ? exception.stack.split("\n")
          : exception.stack;
    }

    if (exception.response) {
      const { response: error } = exception;
      if (error.type === "text") {
        response.send(error.message);
      } else {
        response.status(status).json(error);
      }
    } else {
      response.status(status).json({
        statusCode: status,
        error: exception.name,
        message: exception.message,
        stack: exception.stack,
      });
    }
  }
}
