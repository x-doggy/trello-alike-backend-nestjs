/**
 * @package auth.local
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class local.strategy
 */
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";

import { User } from "~/user/user.entity";
import { UserService } from "~/user/user.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  public constructor(private readonly userService: UserService) {
    super({ usernameField: "email" });
  }

  public async validate(email: string, password: string): Promise<User | never> {
    try {
      return this.userService.validateUser(email, password);
    } catch (e) {
      throw new UnauthorizedException(e.message);
    }
  }
}
