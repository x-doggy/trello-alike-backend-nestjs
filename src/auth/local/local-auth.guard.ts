/**
 * @package auth.local
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class local-auth.guard
 */
import { Injectable } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class LocalAuthGuard extends AuthGuard("local") {}
