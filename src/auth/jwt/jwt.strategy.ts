/**
 * @package auth.jwt
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class jwt.strategy
 */
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";

import { User } from "~/user/user.entity";
import { UserService } from "~/user/user.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  public constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get("JWT_SECRET"),
    });
  }

  public async validate(payload: { sub: string; email: string }): Promise<User | never> {
    return await this.userService.findOne(payload.sub);
  }
}
