/**
 * @package auth.jwt
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class jwt-config.factory
 */
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtModuleOptions, JwtOptionsFactory } from "@nestjs/jwt";

@Injectable()
export class JwtConfigFactory implements JwtOptionsFactory {
  constructor(private readonly configService: ConfigService) {}

  createJwtOptions(): JwtModuleOptions | Promise<JwtModuleOptions> {
    return {
      secret: this.configService.get("JWT_SECRET"),
      signOptions: { expiresIn: this.configService.get("JWT_EXPIRES_IN") },
    };
  }
}
