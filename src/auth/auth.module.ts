/**
 * @package auth
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class auth.module
 */
import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";

import { JwtConfigFactory } from "./jwt/jwt-config.factory";

PassportModule.register({ session: true });

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useClass: JwtConfigFactory,
    }),
  ],
  exports: [JwtModule],
})
export class AuthModule {}
