/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class comment.guard
 */
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";

import { CommentService } from "~/comment/comment.service";
import { ROLES } from "~/role/role.entity";

@Injectable()
export class CommentGuard implements CanActivate {
  public constructor(private readonly commentService: CommentService) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const authUser = request.user;
    if (authUser.role === ROLES.ADMIN) {
      return true;
    }

    const userId = request.params?.userId;
    const columnId = request.params?.columnId;
    const cardId = request.params?.cardId;
    const commentId = request.params?.commentId;

    if (!userId || !columnId || !cardId || !commentId) {
      return false;
    }

    return await this.commentService.checkCommentOwn(commentId, cardId, columnId, userId);
  }
}
