/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class comment.service
 */
import { Injectable, NotFoundException } from "@nestjs/common";

import { CardRepository } from "~/card/card.repository";
import { CommentCreateDto, CommentEditDto, CommentReplyDto } from "~/comment/comment.dto";
import { Comment } from "~/comment/comment.entity";
import { CommentRepository } from "~/comment/comment.repository";

@Injectable()
export class CommentService {
  public constructor(
    private readonly commentRepository: CommentRepository,
    private readonly cardRepository: CardRepository,
  ) {}

  public findByCardId(
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<Comment[]> {
    return this.commentRepository.findByCardId(cardId, columnId, ownerId);
  }

  public findOneByCardId(
    commentId: string,
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<Comment | never> {
    return this.commentRepository.findOneByCardId(commentId, cardId, columnId, ownerId);
  }

  public findOneDescendantsTree(
    parentCommentId: string,
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<Comment> {
    return this.commentRepository.findOneDescendantsTree(
      parentCommentId,
      cardId,
      columnId,
      ownerId,
    );
  }

  public async create(commentData: CommentCreateDto): Promise<Comment> {
    const comment: Comment = this.commentRepository.create(commentData);
    const savedComment: Comment = await this.commentRepository.save(comment);
    return await this.findOneDescendantsTree(
      savedComment.id,
      commentData.cardId,
      commentData.columnId,
      commentData.ownerId,
    );
  }

  public async edit(commentData: CommentEditDto): Promise<Comment | never> {
    const originComment: Comment | undefined = await this.commentRepository.findOne(
      commentData.commentId,
    );

    if (!originComment) {
      throw new NotFoundException("Not Found");
    }

    const savedComment: Comment = await this.cardRepository.save(
      this.commentRepository.merge(originComment, {
        content: commentData.content,
      }),
    );

    return await this.findOneDescendantsTree(
      savedComment.id,
      commentData.cardId,
      commentData.columnId,
      commentData.ownerId,
    );
  }

  public async reply(commentData: CommentReplyDto): Promise<Comment | never> {
    const comment: Comment = this.commentRepository.create({
      ...commentData,
      parent: await this.commentRepository.findOne(commentData.parentId),
    });
    await this.commentRepository.save(comment);
    return await this.findOneDescendantsTree(
      commentData.parentId,
      commentData.cardId,
      commentData.columnId,
      commentData.ownerId,
    );
  }

  public async checkCommentOwn(
    commentId: string,
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<boolean> {
    const comment: Comment | undefined = await this.commentRepository.findOne(commentId);

    if (
      !comment ||
      !comment.card ||
      !comment.card.cardColumn ||
      !comment.card.cardColumn.owner
    ) {
      return false;
    }

    return (
      comment.card.id === cardId &&
      comment.card.cardColumn.id === columnId &&
      comment.card.cardColumn.owner.id === ownerId
    );
  }
}
