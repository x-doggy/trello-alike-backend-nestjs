/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class comment.entity
 */
import { ApiProperty } from "@nestjs/swagger";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Tree,
  TreeChildren,
  TreeParent,
  UpdateDateColumn,
} from "typeorm";

import { Card } from "~/card/card.entity";

const tableName = "comments";

@Entity(tableName)
@Tree("materialized-path")
export class Comment {
  public static tableName = tableName;

  @PrimaryGeneratedColumn("uuid", { name: "id", comment: "comment id" })
  @ApiProperty()
  public id!: string;

  @Column("text", {
    name: "content",
    nullable: true,
    default: "",
    comment: "comment content",
  })
  @ApiProperty()
  public content?: string | null;

  @CreateDateColumn({
    name: "create_timestamp",
    comment: "comment creating date",
  })
  public createTimestamp!: Date | string;

  @UpdateDateColumn({
    name: "update_timestamp",
    comment: "comment updating date",
  })
  public updateTimestamp!: Date | string;

  @Column({ nullable: false })
  public cardId!: string;

  @ManyToOne(() => Card, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    cascade: true,
    nullable: false,
  })
  @JoinColumn({ name: "cardId", referencedColumnName: "id" })
  public card?: Card;

  @TreeChildren()
  public children?: Comment[];

  @Column({ nullable: true })
  public parentId?: string;

  @TreeParent()
  public parent?: Comment | null;

  public get isRoot(): boolean {
    return !this.parent;
  }
}
