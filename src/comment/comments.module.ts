/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class comments.module
 */
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { Card } from "~/card/card.entity";
import { CardRepository } from "~/card/card.repository";
import { CommentController } from "~/comment/comment.controller";
import { Comment } from "~/comment/comment.entity";
import { CommentRepository } from "~/comment/comment.repository";
import { CommentService } from "~/comment/comment.service";

@Module({
  imports: [TypeOrmModule.forFeature([Comment, CommentRepository, Card, CardRepository])],
  controllers: [CommentController],
  providers: [CommentService],
})
export class CommentModule {}
