/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file comment.dto
 */
import { ApiProperty, OmitType } from "@nestjs/swagger";
import { IsDefined, IsNotEmpty, IsString, IsUUID } from "class-validator";

export class CommentCreateDto {
  @IsUUID(4, { message: "Card Id doesn't match UUID" })
  @IsDefined({ message: "Card Id is not defined" })
  @IsNotEmpty({ message: "Card Id is empty" })
  @ApiProperty({ example: "02fba532-d0ff-49bf-809d-1db5ca800aca" })
  public readonly cardId!: string;

  @IsUUID(4, { message: "Column Id doesn't match UUID" })
  @IsDefined({ message: "Column Id is not defined" })
  @IsNotEmpty({ message: "Column Id is empty" })
  @ApiProperty({ example: "0f48253d-43ce-42bb-ad8b-e900379921ed" })
  public readonly columnId!: string;

  @IsUUID(4, { message: "Owner Id doesn't match UUID" })
  @IsDefined({ message: "Owner Id is not defined" })
  @IsNotEmpty({ message: "Owner Id is empty" })
  @ApiProperty({ example: "fd3f2ab7-cbc2-470e-b0e6-4ff31f9c9070" })
  public readonly ownerId!: string;

  @IsString({ message: "Content is not a string" })
  @IsDefined({ message: "Content is not defined" })
  @ApiProperty()
  public readonly content!: string;
}

export class CommentEditDto extends CommentCreateDto {
  @IsUUID(4, { message: "Comment Id doesn't match UUID" })
  @IsDefined({ message: "Comment Id is not defined" })
  @IsNotEmpty({ message: "Comment Id is empty" })
  @ApiProperty()
  public readonly commentId!: string;
}

export class CommentReplyDto extends CommentCreateDto {
  @IsUUID(4, { message: "Parent Id doesn't match UUID" })
  @IsDefined({ message: "Parent Id is not defined" })
  @IsNotEmpty({ message: "Parent Id is empty" })
  @ApiProperty()
  public readonly parentId!: string;
}

export class CommentCreateBody extends OmitType(CommentCreateDto, [
  "cardId",
  "columnId",
  "ownerId",
] as const) {}

export class CommentEditBody extends OmitType(CommentEditDto, [
  "commentId",
  "cardId",
  "columnId",
  "ownerId",
] as const) {}

export class CommentReplyBody extends OmitType(CommentReplyDto, [
  "cardId",
  "columnId",
  "ownerId",
  "parentId",
] as const) {}
