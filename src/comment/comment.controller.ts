/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class comment.controller
 */
import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from "@nestjs/swagger";
import { plainToClass } from "class-transformer";

import { JwtAuthGuard } from "~/auth/jwt/jwt-auth.guard";
import {
  CommentCreateBody,
  CommentCreateDto,
  CommentEditBody,
  CommentEditDto,
  CommentReplyBody,
  CommentReplyDto,
} from "~/comment/comment.dto";
import { Comment } from "~/comment/comment.entity";
import { CommentGuard } from "~/comment/comment.guard";
import { CommentService } from "~/comment/comment.service";

@Controller("/api/v1/users/:userId/columns/:columnId/cards/:cardId/comments")
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags("Comment")
export class CommentController {
  public constructor(private readonly commentService: CommentService) {}

  @Get()
  @ApiOperation({ summary: "Find all comments. Returns list of roots" })
  @ApiOkResponse({ description: "Comments" })
  public findAll(
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) ownerId: string,
  ): Promise<Comment[]> {
    return this.commentService.findByCardId(cardId, columnId, ownerId);
  }

  @Get(":commentId")
  @ApiOperation({ summary: "Find single comment and its tree children" })
  @ApiOkResponse({ description: "Comment" })
  @ApiNotFoundResponse({ description: "Not found" })
  public findOne(
    @Param("commentId", new ParseUUIDPipe({ version: "4" })) commentId: string,
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) ownerId: string,
  ): Promise<Comment | never> {
    return this.commentService.findOneDescendantsTree(
      commentId,
      cardId,
      columnId,
      ownerId,
    );
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: "Create comment" })
  @ApiBody({ type: () => CommentCreateBody })
  @ApiOkResponse({ description: "Comment" })
  @ApiBadRequestResponse({ description: "Incorrect Data" })
  public create(
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) ownerId: string,
    @Body() commentData: CommentCreateBody,
  ): Promise<Comment> {
    return this.commentService.create(
      plainToClass(CommentCreateDto, { ...commentData, cardId, columnId, ownerId }),
    );
  }

  @Post(":commentId/reply")
  @HttpCode(HttpStatus.OK)
  @UseGuards(CommentGuard)
  @ApiOperation({ summary: "Reply to comment" })
  @ApiBody({ type: () => CommentReplyBody })
  @ApiOkResponse({ description: "Comment" })
  @ApiBadRequestResponse({ description: "Incorrect Data" })
  public reply(
    @Param("commentId", new ParseUUIDPipe({ version: "4" })) commentId: string,
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) ownerId: string,
    @Body() commentData: CommentReplyBody,
  ): Promise<Comment> {
    return this.commentService.reply(
      plainToClass(CommentReplyDto, {
        ...commentData,
        parentId: commentId,
        cardId,
        columnId,
        ownerId,
      }),
    );
  }

  @Patch(":commentId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(CommentGuard)
  @ApiOperation({ summary: "Edit comment" })
  @ApiBody({ type: () => CommentEditBody })
  @ApiOkResponse({ description: "Comment" })
  @ApiNotFoundResponse({ description: "Not found" })
  @ApiForbiddenResponse({ description: "Access is forbidden" })
  public edit(
    @Param("commentId", new ParseUUIDPipe({ version: "4" })) commentId: string,
    @Param("cardId", new ParseUUIDPipe({ version: "4" })) cardId: string,
    @Param("columnId", new ParseUUIDPipe({ version: "4" })) columnId: string,
    @Param("userId", new ParseUUIDPipe({ version: "4" })) ownerId: string,
    @Body() commentData: CommentEditBody,
  ): Promise<Comment | never> {
    return this.commentService.edit(
      plainToClass(CommentEditDto, {
        ...commentData,
        commentId,
        cardId,
        columnId,
        ownerId,
      }),
    );
  }
}
