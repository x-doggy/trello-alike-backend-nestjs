/**
 * @package comment
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class comment.repository
 */
import { EntityRepository, SelectQueryBuilder, TreeRepository } from "typeorm";

import { Card } from "~/card/card.entity";
import { CardColumn } from "~/card-column/card-column.entity";
import { Comment } from "~/comment/comment.entity";

@EntityRepository(Comment)
export class CommentRepository extends TreeRepository<Comment> {
  public select(): SelectQueryBuilder<Comment> {
    return this.createQueryBuilder(Comment.tableName)
      .leftJoin(`${Comment.tableName}.card`, Card.tableName)
      .leftJoin(`${Card.tableName}.cardColumn`, CardColumn.tableName)
      .select([
        `${Comment.tableName}.id`,
        `${Comment.tableName}.content`,
        `${Comment.tableName}.createTimestamp`,
        `${Comment.tableName}.updateTimestamp`,
      ])
      .orderBy(`${Comment.tableName}.createTimestamp`, "ASC");
  }

  public findByCardId(
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<Comment[]> {
    return this.select()
      .where(`${Comment.tableName}.cardId = :cardId`, { cardId })
      .andWhere(`${Comment.tableName}.parentId IS NULL`)
      .andWhere(`${Card.tableName}.cardColumnId = :columnId`, { columnId })
      .andWhere(`${CardColumn.tableName}.ownerId = :ownerId`, { ownerId })
      .getMany();
  }

  public findOneByCardId(
    commentId: string,
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<Comment | never> {
    return this.select()
      .where(`${Comment.tableName}.id = :commentId`, { commentId })
      .andWhere(`${Comment.tableName}.cardId = :cardId`, { cardId })
      .andWhere(`${Card.tableName}.cardColumnId = :columnId`, { columnId })
      .andWhere(`${CardColumn.tableName}.ownerId = :ownerId`, { ownerId })
      .getOneOrFail();
  }

  public async findOneDescendantsTree(
    parentCommentId: string,
    cardId: string,
    columnId: string,
    ownerId: string,
  ): Promise<Comment> {
    const comment = await this.findOneByCardId(
      parentCommentId,
      cardId,
      columnId,
      ownerId,
    );
    return await this.findDescendantsTree(comment);
  }
}
