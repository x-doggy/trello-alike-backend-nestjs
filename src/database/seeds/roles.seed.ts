/**
 * @package database.seeds
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class roles.seed
 */
import { Connection } from "typeorm";
import { Factory, Seeder } from "typeorm-seeding";

import { Role } from "~/role/role.entity";

export default class CreateRoles implements Seeder {
  public async run(_factory: Factory, connection: Connection): Promise<void> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(Role)
      .values([{ name: "ADMIN" }, { name: "USER" }])
      .execute();
  }
}
