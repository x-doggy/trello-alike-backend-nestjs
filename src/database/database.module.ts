/**
 * @package database
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class database.module
 */
import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => {
        const entitiesConfig = config.get("TYPEORM_ENTITIES") as string;
        const entitiesDir = entitiesConfig
          .replace(/src\//, "dist/")
          .replace(/.ts/, "{.ts, .js}");
        return {
          type: "postgres",
          autoLoadEntities: true,
          host: config.get("TYPEORM_HOST"),
          port: config.get<number>("TYPEORM_PORT"),
          username: config.get("TYPEORM_USERNAME"),
          password: config.get("TYPEORM_PASSWORD"),
          database: config.get("TYPEORM_DATABASE"),
          synchronize: config.get<boolean>("TYPEORM_SYNCHRONIZE"),
          logging: config.get<boolean>("TYPEORM_LOGGING"),
          entities: [entitiesDir],
        };
      },
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
