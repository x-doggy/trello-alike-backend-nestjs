/**
 * @package role
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file roles.decorator
 */
import { SetMetadata } from "@nestjs/common";

import { ROLES } from "~/role/role.entity";

export const ROLES_KEY = "role";
export const Roles = (role: ROLES) => SetMetadata(ROLES_KEY, role);
