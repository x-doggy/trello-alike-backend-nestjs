/**
 * @package role
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class role.repository
 */
import { EntityRepository, Repository, SelectQueryBuilder } from "typeorm";

import { Role, ROLES } from "~/role/role.entity";

@EntityRepository(Role)
export class RoleRepository extends Repository<Role> {
  public select(): SelectQueryBuilder<Role> {
    return this.createQueryBuilder(Role.tableName).select([
      `${Role.tableName}.id`,
      `${Role.tableName}.name`,
      `${Role.tableName}.createTimestamp`,
      `${Role.tableName}.updateTimestamp`,
    ]);
  }

  public findByName(name: string): Promise<Role | never> {
    return this.select().where(`${Role.tableName}.name = :name`, { name }).getOneOrFail();
  }

  public findByNames(names: string[]): Promise<Role[]> {
    return this.select().where("role.name IN (:...names)", { names }).getMany();
  }

  public findUserRole(): Promise<Role> {
    return this.findByName(ROLES.USER);
  }

  public findAdminRole(): Promise<Role> {
    return this.findByName(ROLES.ADMIN);
  }
}
