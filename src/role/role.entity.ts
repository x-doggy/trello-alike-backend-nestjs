/**
 * @package role
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @class role.entity
 */
import { ApiProperty } from "@nestjs/swagger";
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from "typeorm";

export enum ROLES {
  USER = "USER",
  ADMIN = "ADMIN",
}

const tableName = "roles";

@Entity(tableName)
@Unique(["id"])
export class Role {
  public static tableName = tableName;

  @PrimaryGeneratedColumn("uuid", { name: "id", comment: "role id" })
  @ApiProperty()
  public id!: string;

  @Column("varchar", {
    name: "name",
    unique: true,
    nullable: false,
    length: 120,
  })
  @ApiProperty()
  public name!: string;

  @CreateDateColumn({
    name: "create_timestamp",
    comment: "role creating date",
  })
  public createTimestamp!: Date | string;

  @UpdateDateColumn({
    name: "update_timestamp",
    comment: "role updating date",
  })
  public updateTimestamp!: Date | string;

  public constructor(name: string) {
    this.name = name;
  }
}
