/**
 * @package env
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file env.validation
 */
import { plainToClass, Transform } from "class-transformer";
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsNumber,
  IsString,
  validateSync,
} from "class-validator";

function transformBooleanString({ value }: { value: unknown }): boolean {
  if (value === "true") {
    return true;
  }
  if (value === "false") {
    return false;
  }
  return Boolean(value);
}

enum MODE_ENV {
  DEV = "dev",
  PROD = "prod",
}

class EnvironmentVariables {
  @IsEnum(MODE_ENV, {
    message: "Incorrect MODE option is set. Only 'dev' or 'prod' values are valid",
  })
  @IsDefined()
  MODE!: MODE_ENV;

  @IsString()
  @IsDefined()
  JWT_SECRET!: string;

  @IsString()
  @IsDefined()
  JWT_EXPIRES_IN!: string;

  @IsNumber()
  NEST_HTTP_PORT!: number;

  @IsString()
  TYPEORM_CONNECTION!: string;

  @IsString()
  @IsDefined()
  TYPEORM_HOST!: string;

  @IsNumber()
  @IsDefined()
  TYPEORM_PORT!: number;

  @IsString()
  @IsDefined()
  TYPEORM_USERNAME!: string;

  @IsString()
  @IsDefined()
  TYPEORM_PASSWORD!: string;

  @IsString()
  @IsDefined()
  TYPEORM_DATABASE!: string;

  @Transform(transformBooleanString)
  @IsBoolean()
  TYPEORM_SYNCHRONIZE!: string;

  @Transform(transformBooleanString)
  @IsBoolean()
  TYPEORM_LOGGING!: string;

  @IsString()
  @IsDefined()
  TYPEORM_ENTITIES!: string;

  @IsString()
  @IsDefined()
  TYPEORM_MIGRATIONS!: string;

  @IsString()
  @IsDefined()
  TYPEORM_MIGRATIONS_DIR!: string;

  @IsString()
  @IsDefined()
  TYPEORM_SEEDING_FACTORIES!: string;

  @IsString()
  @IsDefined()
  TYPEORM_SEEDING_SEEDS!: string;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, { skipMissingProperties: false });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
