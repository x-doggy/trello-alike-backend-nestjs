/**
 * @package
 * @author Vladimir Stadnik <vladimir.stadnik@purrweb.com>
 * @file main
 */
import { ConfigService } from "@nestjs/config";
import { NestFactory } from "@nestjs/core";
import { ExpressAdapter, NestExpressApplication } from "@nestjs/platform-express";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

import { AppModule } from "~/app.module";

(async () => {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter(),
  );

  const documentOptions = new DocumentBuilder()
    .setTitle("Trello-alike")
    .setDescription("Trello-alike NestJS application for Purrweb")
    .setVersion("1.0")
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, documentOptions);
  SwaggerModule.setup("api/v1/swagger/documentation", app, document);

  app.enableCors({
    origin: "*",
    maxAge: 1728000,
    credentials: true,
    methods: ["GET", "POST", "PUT", "PATCH", "OPTIONS", "DELETE"],
  });

  const configService = app.get(ConfigService);
  const serverPort = configService.get<number>("NEST_HTTP_PORT") as number;

  await app.listen(serverPort);
})();
